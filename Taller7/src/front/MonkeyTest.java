package front;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import util.Constantes;
import util.DistribucionEjecutada;
import util.TelnetService;

public class MonkeyTest extends JFrame implements ActionListener {
    
	private JPanel contentPane;
	private JTextField textFieldNroEventos;
	private JList<String> listComandos;
	private JList<String> listComandosUsar;
	private DefaultListModel<String> modelComandos;
	private DefaultListModel<String> modelComandosUsar;
	private JTextField textFieldRutaApk;
	private JTextField textFieldPaquete;
	private Runtime rt = Runtime.getRuntime();
	private JTextField txtRutaADB;
	private JTextField textFieldTelnetHost;
	private JTextField textFieldPuerto;
	private JTextField textFieldAuth;
	private Boolean primeraVez = true;
	private JTextField textFieldDistribucion;
	private Map<String, DistribucionEjecutada> mapaDistribucion;
	private JTextArea textAreaConsola;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MonkeyTest frame = new MonkeyTest();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MonkeyTest() {
		setTitle("Monkey - Taller 7");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 780);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Comandos", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(37, 254, 733, 276);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("N\u00FAmero de Eventos");
		lblNewLabel.setBounds(35, 30, 159, 16);
		panel.add(lblNewLabel);
		
		textFieldNroEventos = new JTextField();
		textFieldNroEventos.setBounds(163, 27, 135, 22);
		panel.add(textFieldNroEventos);
		textFieldNroEventos.setColumns(10);
		
		JLabel lblComandosAEjecutar = new JLabel("Seleccione Comandos");
		lblComandosAEjecutar.setBounds(35, 73, 159, 16);
		panel.add(lblComandosAEjecutar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(59, 102, 225, 91);
		panel.add(scrollPane);
		
		listComandos = new JList<>();
		scrollPane.setViewportView(listComandos);
		modelComandos = new DefaultListModel<String>();
		listComandos.setModel(modelComandos);
		String[] values = Constantes.COMANDOS.keySet().toArray(new String[] {});
		for (String v : values) {
			modelComandos.addElement(v);
			
		}
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(458, 102, 225, 91);
		panel.add(scrollPane_1);
		
		listComandosUsar = new JList<>();
		scrollPane_1.setViewportView(listComandosUsar);
		listComandosUsar.setVisibleRowCount(3);
		modelComandosUsar = new DefaultListModel<String>();
		listComandosUsar.setModel(modelComandosUsar);
		
		JLabel label = new JLabel("Comandos a usar");
		label.setBounds(440, 73, 159, 16);
		panel.add(label);
		
		JButton btnPasar = new JButton(">");
		btnPasar.addActionListener(this);
		btnPasar.setBounds(347, 86, 56, 25);
		panel.add(btnPasar);
		
		JButton btnPasarTodo = new JButton(">>");
		btnPasarTodo.addActionListener(this);
		btnPasarTodo.setBounds(347, 113, 56, 25);
		panel.add(btnPasarTodo);
		
		JButton btnQuitarTodo = new JButton("<<");
		btnQuitarTodo.addActionListener(this);
		btnQuitarTodo.setBounds(347, 140, 56, 25);
		panel.add(btnQuitarTodo);
		
		JButton btnQuitar = new JButton("<");
		btnQuitar.addActionListener(this);
		btnQuitar.setBounds(347, 168, 56, 25);
		panel.add(btnQuitar);
		
		JButton btnEjecutar = new JButton("Ejecutar");
		btnEjecutar.addActionListener(this);
		btnEjecutar.setBounds(318, 241, 97, 25);
		panel.add(btnEjecutar);
		
		JLabel lblDistribucin = new JLabel("Distribuci\u00F3n");
		lblDistribucin.setBounds(35, 209, 235, 16);
		panel.add(lblDistribucin);
		
		textFieldDistribucion = new JTextField();
		textFieldDistribucion.setToolTipText("Forma: 0.4,0.4,0.2");
		textFieldDistribucion.setColumns(10);
		textFieldDistribucion.setBounds(162, 206, 536, 22);
		panel.add(textFieldDistribucion);
		
		JLabel lblForma = new JLabel("Forma: 0.4,0.4,0.2");
		lblForma.setBounds(44, 225, 235, 16);
		panel.add(lblForma);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Instalacion APK", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(37, 134, 733, 114);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel label_1 = new JLabel("Ruta APK a instalar");
		label_1.setBounds(30, 25, 235, 16);
		panel_1.add(label_1);
		
		textFieldRutaApk = new JTextField();
		textFieldRutaApk.setText("C:/APK/\"Omni Notes_v5.3.2_apkpure.com.apk\"");
		textFieldRutaApk.setColumns(10);
		textFieldRutaApk.setBounds(40, 54, 665, 22);
		panel_1.add(textFieldRutaApk);
		
		JLabel lblPaqueteMainactivity = new JLabel("Paquete MainActivity");
		lblPaqueteMainactivity.setBounds(30, 85, 235, 16);
		panel_1.add(lblPaqueteMainactivity);
		
		textFieldPaquete = new JTextField();
		textFieldPaquete.setText("it.feio.android.omninotes");
		textFieldPaquete.setColumns(10);
		textFieldPaquete.setBounds(209, 82, 496, 22);
		panel_1.add(textFieldPaquete);
		
		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBorder(new TitledBorder(null, "Configuracion ADB y Telnet", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(37, 13, 733, 114);
		contentPane.add(panel_2);
		
		JLabel lblRutaAdb = new JLabel("Ruta ADB");
		lblRutaAdb.setBounds(30, 28, 235, 16);
		panel_2.add(lblRutaAdb);
		
		txtRutaADB = new JTextField();
		txtRutaADB.setText("C:/Users/Edgar-pc/AppData/Local/Android/sdk/platform-tools/");
		txtRutaADB.setColumns(10);
		txtRutaADB.setBounds(157, 25, 554, 22);
		panel_2.add(txtRutaADB);
		
		JLabel lblHost = new JLabel("Telnet Host");
		lblHost.setBounds(30, 57, 125, 16);
		panel_2.add(lblHost);
		
		textFieldTelnetHost = new JTextField();
		textFieldTelnetHost.setText("localhost");
		textFieldTelnetHost.setColumns(10);
		textFieldTelnetHost.setBounds(157, 54, 220, 22);
		panel_2.add(textFieldTelnetHost);
		
		JLabel lblPuerto = new JLabel("Telnet Puerto");
		lblPuerto.setBounds(389, 57, 111, 16);
		panel_2.add(lblPuerto);
		
		textFieldPuerto = new JTextField();
		textFieldPuerto.setText("5554");
		textFieldPuerto.setColumns(10);
		textFieldPuerto.setBounds(491, 54, 220, 22);
		panel_2.add(textFieldPuerto);
		
		JLabel lblTelnetAuth = new JLabel("Telnet Auth");
		lblTelnetAuth.setBounds(30, 89, 125, 16);
		panel_2.add(lblTelnetAuth);
		
		textFieldAuth = new JTextField();
		textFieldAuth.setText("8dCa47wjLdhmejlM");
		textFieldAuth.setColumns(10);
		textFieldAuth.setBounds(157, 86, 220, 22);
		panel_2.add(textFieldAuth);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(89, 552, 664, 99);
		contentPane.add(scrollPane_2);
		
		textAreaConsola = new JTextArea();
		scrollPane_2.setViewportView(textAreaConsola);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof JButton) {
			JButton b = (JButton)e.getSource();
			if (b.getText().equals(">")) {
				Object[] comandos = (Object[])this.listComandos.getSelectedValues();
				Object[] comandosSel = this.modelComandosUsar.toArray();
				if (comandos != null && comandos.length > 0) {
					for (Object comando : comandos) {
						boolean esta = false;
						if (comandosSel != null && comandosSel.length > 0) {
							for (Object comandoSel : comandosSel) {
								if (comando.equals(comandoSel)) {
									esta = true;
									break;
								}
							}
						}
						if (!esta) {
							this.modelComandosUsar.addElement((String)comando);
						}
					}
				}
			}else if (b.getText().equals(">>")) {
				Object[] comandos = this.modelComandos.toArray();
				Object[] comandosSel = this.modelComandosUsar.toArray();
				if (comandos != null && comandos.length > 0) {
					for (Object comando : comandos) {
						boolean esta = false;
						if (comandosSel != null && comandosSel.length > 0) {
							for (Object comandoSel : comandosSel) {
								if (comando.equals(comandoSel)) {
									esta = true;
									break;
								}
							}
						}
						if (!esta) {
							this.modelComandosUsar.addElement((String)comando);
						}
					}
				}
			}else if (b.getText().equals("<<")) {
				modelComandosUsar.clear();
			}else if (b.getText().equals("<")) {
				Object[] comandos = (Object[])this.listComandosUsar.getSelectedValues();
				if (comandos != null && comandos.length > 0) {
					for (Object comando : comandos) {
						this.modelComandosUsar.removeElement(comando);
					}
				}
			}else if(b.getText().equalsIgnoreCase("ejecutar")) {
				if (camposObligatorios()) {
					String[] distribucion = textFieldDistribucion.getText().split(",");
					Object[] comandos = (Object[])this.modelComandosUsar.toArray();
					float suma = 0;
					for (String d : distribucion) {
						suma += Float.valueOf(d.trim());
					}
					if (suma == 1.0) {
						if (distribucion.length == comandos.length) {
							if (primeraVez) {
								
								iniciar();
								primeraVez = false;
							}
							ejecutar();
						}else {
							JOptionPane.showMessageDialog(null, "ERROR La distribucion debe ser igual al numero de comandos a usar");
						}
					}
					else {
						JOptionPane.showMessageDialog(null, "ERROR La suma de las distribuciones debe ser igual 1 ");
					}
				}else {
					JOptionPane.showMessageDialog(null, "ERROR campos obligatorios");
				}
			}
		}		
	}

	private boolean camposObligatorios() {
		boolean obligatorio = true;
		if (textFieldAuth.getText() == null || textFieldAuth.getText().isEmpty() ||
				textFieldNroEventos.getText() == null || textFieldNroEventos.getText().isEmpty() || 
				textFieldPaquete.getText() == null || textFieldPaquete.getText().isEmpty() ||
				textFieldRutaApk.getText() == null || textFieldRutaApk.getText().isEmpty() ||
				textFieldTelnetHost.getText() == null || textFieldTelnetHost.getText().isEmpty() ||
				textFieldPuerto.getText() == null || textFieldPuerto.getText().isEmpty() ||
				txtRutaADB.getText() == null || txtRutaADB.getText().isEmpty() ||
				textFieldDistribucion.getText() == null || textFieldDistribucion.getText().isEmpty()) {
			obligatorio = false;
		}
		return obligatorio;
	}

	private void iniciar() {
		try {
			Process adbUninstall = rt.exec(txtRutaADB.getText()+Constantes.ADB_UNINSTALL_APK+textFieldPaquete.getText());
			adbUninstall.waitFor();
			Process adbInstall = rt.exec(txtRutaADB.getText()+Constantes.ADB_INSTALL_APK+textFieldRutaApk.getText());
			adbInstall.waitFor();
			Process adbStart = rt.exec(txtRutaADB.getText()+Constantes.ADB_START_APP_1+textFieldPaquete.getText()+Constantes.ADB_START_APP_2);
			adbStart.waitFor();
		}catch(Exception exc) {
			JOptionPane.showMessageDialog(null, "ERROR "+exc.getMessage());
		}
	}
	
	private void ejecutar() {
		
		try {
			int numeroEventos = Integer.valueOf(textFieldNroEventos.getText()); 
	        Random random = new Random(12345);
	        mapaDistribucion = new HashMap<>();
	        Constantes.generarDistribucion(mapaDistribucion, this.modelComandosUsar.toArray(), this.textFieldDistribucion.getText().split(","), numeroEventos);
	        System.out.println(mapaDistribucion);
	        textAreaConsola.setText(textAreaConsola.getText()+mapaDistribucion+"\n");
			for (int i=0;i<numeroEventos;i++) {
				Object comando = Constantes.obtenerComandoRandom(this.modelComandosUsar.toArray(),mapaDistribucion);
				String argumentos = "";
				String sentencia = Constantes.COMANDOS.get(comando);
				if (comando.toString().equals("tap")) {
					int x = random.nextInt(Constantes.ANCHO);
	                int y = random.nextInt(Constantes.ALTO);
	                argumentos = x + " " + y;
				}else if (comando.toString().equals("swipe")) {
					int x1 = random.nextInt(Constantes.ANCHO);
	                int y1 = random.nextInt(Constantes.ALTO);
	                int x2 = random.nextInt(Constantes.ANCHO);
	                int y2 = random.nextInt(Constantes.ALTO);
	                argumentos = x1 + " " + y1 + " " + x2 + " " + y2;
				}else if (comando.toString().equals("text")) {
					argumentos = Constantes.getPalabra(6);
				}else if (comando.toString().equals("keyevent")) {
					argumentos = Constantes.getKeyevent() + "";
				}else if (comando.toString().equals("rotate")) {
					TelnetService.rotate(textFieldTelnetHost.getText(), Integer.valueOf(textFieldPuerto.getText()), textFieldAuth.getText());
				}else if (comando.toString().equals("speed")) {
					String velociodad = Constantes.obtenerVelocidadRandom();
					TelnetService.speed(textFieldTelnetHost.getText(), Integer.valueOf(textFieldPuerto.getText()), textFieldAuth.getText(), sentencia, velociodad);
				}else if (comando.toString().equals("acceleration")) {
					TelnetService.acceleration(textFieldTelnetHost.getText(), Integer.valueOf(textFieldPuerto.getText()), textFieldAuth.getText(), sentencia);
				}
				if (sentencia.contains("adb")) {
					rt.exec(txtRutaADB.getText()+sentencia+argumentos);
				}
				System.out.println("sentencia "+sentencia);
				textAreaConsola.setText(textAreaConsola.getText()+("sentencia "+sentencia)+"\n");
				Thread.sleep(3000);
			}
	        System.out.println(mapaDistribucion);
			textAreaConsola.setText(textAreaConsola.getText()+mapaDistribucion+"\n");
		}catch(Exception e) {
			JOptionPane.showMessageDialog(null, "ERROR "+e.getMessage());
		}
	}
}