package util;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Constantes {
	
	public static final Map<String, String> COMANDOS = new HashMap<>();
	public static final Integer[] KEYEVENTS = new Integer[] {
			75, //Apostrophe
			73, //Backslash
			67, //Backspace
			164,//Volume Mute
			25, //Volume Down
			24, //Volume Up
			220,//Brigthness Down
			221,//Brigthness Up
			51, //Keycode W
			72 //Right bracket
	};

	static {
		COMANDOS.put("tap", "adb shell input tap ");
		COMANDOS.put("text", "adb shell input text ");
		COMANDOS.put("swipe", "adb shell input swipe ");
		COMANDOS.put("keyevent", "adb shell input keyevent ");
		COMANDOS.put("rotate", "rotate");
		COMANDOS.put("speed", "network speed ");
		COMANDOS.put("acceleration", "sensor set acceleration 2.23517e-07:9.77631:0.812348");
	}
	
	public static final String ALFANUMERICO = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public static final String ADB_UNINSTALL_APK = "adb uninstall ";
	public static final String ADB_INSTALL_APK = "adb install ";
	public static final String ADB_START_APP_1 = "adb shell am start -n ";
	public static final String ADB_START_APP_2 = "/.MainActivity";
	public static final Integer ANCHO = 1080;
	public static final Integer ALTO = 1920;
    
	public enum VELOCITY{
		 gsm,
		 hscsd,
		 gprs,
		 edge,
		 umts,
		 hsdpa,
		 lte,
		 evdo,
		 full
	}
	
	private Constantes() {
	}
	
	public static String getLetra() {
		Random r = new Random();
		return String.valueOf(ALFANUMERICO.charAt(r.nextInt(ALFANUMERICO.length())));
	}
	
	public static String getPalabra(int tam) {
		String palabra = "";
		for (int i=0;i<tam;i++) {
			palabra+=getLetra();
		}
		return palabra;
	}
	
	public static Integer getKeyevent() {
		Random r = new Random();
		return KEYEVENTS[r.nextInt(KEYEVENTS.length -1)];
	}

	public static String obtenerVelocidadRandom() {
		Random r = new Random();
		return VELOCITY.values()[r.nextInt(VELOCITY.values().length)].name();
	}

	public static void generarDistribucion(Map<String, DistribucionEjecutada> mapaDistribucion, Object[] comandos, String[] distribucion,
			int numeroEventos) {
		for (int i=0;i<comandos.length;i++) {
			Object comando = comandos[i];
			DistribucionEjecutada de = new DistribucionEjecutada();
			de.setComando((String)comando);
			de.setReciente(0);
			Double dis = numeroEventos * Double.valueOf(distribucion[i].trim());
			de.setMaximo(Math.round(dis.floatValue()));
			mapaDistribucion.put((String)comando, de);
		}
	}

	public static Object obtenerComandoRandom(Object[] comandosUsar, Map<String, DistribucionEjecutada> mapaDistribucion) {
		Object comando = null;
		Random r = new Random();
		boolean band = false;
		int i = 0;
		while (!band || i == 10) {
			comando = comandosUsar[r.nextInt(comandosUsar.length)];
			DistribucionEjecutada d = mapaDistribucion.get(comando);
			if (d.getReciente() < d.getMaximo()) {
				band = true;
				d.setReciente(d.getReciente() + 1);
			}
			i++;
		}
		
		return comando;
	}
}
