package util;

public class DistribucionEjecutada {
	private String comando;
	private Integer reciente;
	private Integer maximo;
	
	public String getComando() {
		return comando;
	}
	public void setComando(String comando) {
		this.comando = comando;
	}
	public Integer getReciente() {
		return reciente;
	}
	public void setReciente(Integer reciente) {
		this.reciente = reciente;
	}
	public Integer getMaximo() {
		return maximo;
	}
	public void setMaximo(Integer maximo) {
		this.maximo = maximo;
	}
	@Override
	public String toString() {
		return "DistribucionEjecutada [comando=" + comando + ", reciente=" + reciente + ", maximo=" + maximo + "]";
	}
}
