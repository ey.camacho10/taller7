package util;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

import org.apache.commons.net.telnet.TelnetClient;

public class TelnetService {
	
	private TelnetService() {
	}
	
    private static TelnetClient telnet = null;
    
    public static BufferedWriter connectToTelnet(String host, Integer port) throws IOException {
    	telnet = new TelnetClient();
    	telnet.connect(host, port);
        return new BufferedWriter(new OutputStreamWriter(telnet.getOutputStream()));
    }

	public static void rotate(String host, Integer port, String token) throws IOException {
        BufferedWriter out = connectToTelnet(host, port);
        out.write("auth "+token+"\n");
        out.write("rotate\n");
        out.write("quit\n");
        out.flush();
    }
	
	public static void speed(String host, Integer port, String token, String comando, String speed) throws IOException {
        BufferedWriter out = connectToTelnet(host, port);
        out.write("auth "+token+"\n");
        out.write(comando+speed+"\n");
        out.write("quit\n");
        out.flush();
    }
	
	public static void acceleration(String host, Integer port, String token, String comando) throws IOException {
        BufferedWriter out = connectToTelnet(host, port);
        out.write("auth "+token+"\n");
        out.write(comando+"\n");
        out.write("quit\n");
        out.flush();
    }
}