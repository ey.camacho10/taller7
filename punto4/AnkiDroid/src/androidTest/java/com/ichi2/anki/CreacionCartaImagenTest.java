package com.ichi2.anki;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class CreacionCartaImagenTest {

    @Rule
    public ActivityTestRule<IntentHandler> mActivityTestRule = new ActivityTestRule<>(IntentHandler.class);

    @Test
    public void creacionCartaImagenTest() {
        ViewInteraction viewInteraction = onView(
                allOf(withId(R.id.fab_expand_menu_button), withContentDescription("Add"),
                        withParent(allOf(withId(R.id.add_content_menu),
                                withParent(withId(R.id.root_layout)))),
                        isDisplayed()));
        viewInteraction.perform(click());

        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.add_note_action), withContentDescription("Add"),
                        withParent(allOf(withId(R.id.add_content_menu),
                                withParent(withId(R.id.root_layout)))),
                        isDisplayed()));
        floatingActionButton.perform(click());

        ViewInteraction appCompatSpinner = onView(
                withId(R.id.note_deck_spinner));
        appCompatSpinner.perform(scrollTo(), click());

        ViewInteraction appCompatCheckedTextView = onView(
                allOf(withId(android.R.id.text1), withText("deckprueba"), isDisplayed()));
        appCompatCheckedTextView.perform(click());

        ViewInteraction appCompatImageButton = onView(
                allOf(withId(R.id.id_media_button), withContentDescription("Attach multimedia content to the Front field")));
        appCompatImageButton.perform(scrollTo(), click());

        ViewInteraction appCompatTextView = onView(
                allOf(withId(R.id.title), withText("Add image"), isDisplayed()));
        appCompatTextView.perform(click());

        ViewInteraction button = onView(
                allOf(withText("Gallery"),
                        withParent(allOf(withId(R.id.LinearLayoutInScrollViewFieldEdit),
                                withParent(withId(R.id.scrollView1))))));
        button.perform(scrollTo(), click());

        ViewInteraction button2 = onView(
                allOf(withText("Gallery"),
                        withParent(allOf(withId(R.id.LinearLayoutInScrollViewFieldEdit),
                                withParent(withId(R.id.scrollView1))))));
        button2.perform(scrollTo(), click());

        ViewInteraction actionMenuItemView = onView(
                allOf(withId(R.id.multimedia_edit_field_done), withContentDescription("Done"), isDisplayed()));
        actionMenuItemView.perform(click());

        ViewInteraction fieldEditText = onView(
                allOf(withId(R.id.id_note_editText), withContentDescription("Front")));
        fieldEditText.perform(scrollTo(), replaceText("<img src='Screenshot_20170928-014248.png'/>"), closeSoftKeyboard());

        ViewInteraction fieldEditText2 = onView(
                allOf(withId(R.id.id_note_editText), withContentDescription("Back")));
        fieldEditText2.perform(scrollTo(), replaceText("prueba"), closeSoftKeyboard());

        ViewInteraction actionMenuItemView2 = onView(
                allOf(withId(R.id.action_save), withContentDescription("Save"), isDisplayed()));
        actionMenuItemView2.perform(click());

        ViewInteraction appCompatImageButton2 = onView(
                allOf(withContentDescription("Navigate up"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton2.perform(click());

        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.files),
                        withParent(allOf(withId(R.id.pull_to_sync_wrapper),
                                withParent(withId(R.id.deckpicker_view)))),
                        isDisplayed()));
        recyclerView.perform(actionOnItemAtPosition(0, click()));

        ViewInteraction appCompatImageButton3 = onView(
                allOf(withContentDescription("Navigate up"),
                        withParent(allOf(withId(R.id.toolbar),
                                withParent(withId(R.id.front_frame)))),
                        isDisplayed()));
        appCompatImageButton3.perform(click());

        ViewInteraction appCompatCheckedTextView2 = onView(
                allOf(withId(R.id.design_menu_item_text), withText("Decks"), isDisplayed()));
        appCompatCheckedTextView2.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction viewInteraction2 = onView(
                allOf(withId(R.id.fab_expand_menu_button), withContentDescription("Add"),
                        withParent(allOf(withId(R.id.add_content_menu),
                                withParent(withId(R.id.root_layout)))),
                        isDisplayed()));
        viewInteraction2.perform(click());

    }

}
