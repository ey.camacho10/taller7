package com.ichi2.anki;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class CrearDeckYCartaTest {

    @Rule
    public ActivityTestRule<IntentHandler> mActivityTestRule = new ActivityTestRule<>(IntentHandler.class);

    @Test
    public void crearDeckYCartaTest() {
        ViewInteraction viewInteraction = onView(
                allOf(withId(R.id.fab_expand_menu_button), withContentDescription("Add"),
                        withParent(allOf(withId(R.id.add_content_menu),
                                withParent(withId(R.id.root_layout)))),
                        isDisplayed()));
        viewInteraction.perform(click());

        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.add_deck_action), withContentDescription("Create deck"),
                        withParent(allOf(withId(R.id.add_content_menu),
                                withParent(withId(R.id.root_layout)))),
                        isDisplayed()));
        floatingActionButton.perform(click());

        ViewInteraction editText = onView(
                withClassName(is("android.widget.EditText")));
        editText.perform(scrollTo(), click());

        ViewInteraction editText2 = onView(
                withClassName(is("android.widget.EditText")));
        editText2.perform(scrollTo(), replaceText("deck_1"), closeSoftKeyboard());

        ViewInteraction mDButton = onView(
                allOf(withId(R.id.buttonDefaultPositive), withText("OK"),
                        withParent(allOf(withId(R.id.root),
                                withParent(withId(android.R.id.content)))),
                        isDisplayed()));
        mDButton.perform(click());

        ViewInteraction viewInteraction2 = onView(
                allOf(withId(R.id.fab_expand_menu_button), withContentDescription("Add"),
                        withParent(allOf(withId(R.id.add_content_menu),
                                withParent(withId(R.id.root_layout)))),
                        isDisplayed()));
        viewInteraction2.perform(click());

        ViewInteraction floatingActionButton2 = onView(
                allOf(withId(R.id.add_note_action), withContentDescription("Add"),
                        withParent(allOf(withId(R.id.add_content_menu),
                                withParent(withId(R.id.root_layout)))),
                        isDisplayed()));
        floatingActionButton2.perform(click());

        ViewInteraction fieldEditText = onView(
                allOf(withId(R.id.id_note_editText), withContentDescription("Front")));
        fieldEditText.perform(scrollTo(), replaceText("pre"), closeSoftKeyboard());

        ViewInteraction fieldEditText2 = onView(
                allOf(withId(R.id.id_note_editText), withContentDescription("Back")));
        fieldEditText2.perform(scrollTo(), replaceText("res"), closeSoftKeyboard());

        ViewInteraction actionMenuItemView = onView(
                allOf(withId(R.id.action_save), withContentDescription("Save"), isDisplayed()));
        actionMenuItemView.perform(click());

        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Navigate up"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction textView = onView(
                allOf(withId(R.id.deckpicker_name), withText("deck_1"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.DeckPickerHoriz),
                                        1),
                                0),
                        isDisplayed()));
        //textView.check(matches(withText("deck_1")));

        ViewInteraction viewInteraction3 = onView(
                allOf(withId(R.id.fab_expand_menu_button), withContentDescription("Add"),
                        withParent(allOf(withId(R.id.add_content_menu),
                                withParent(withId(R.id.root_layout)))),
                        isDisplayed()));
        viewInteraction3.perform(click());

        ViewInteraction floatingActionButton3 = onView(
                allOf(withId(R.id.add_note_action), withContentDescription("Add"),
                        withParent(allOf(withId(R.id.add_content_menu),
                                withParent(withId(R.id.root_layout)))),
                        isDisplayed()));
        floatingActionButton3.perform(click());

        ViewInteraction appCompatSpinner = onView(
                withId(R.id.note_deck_spinner));
        appCompatSpinner.perform(scrollTo(), click());

        ViewInteraction appCompatCheckedTextView = onView(
                allOf(withId(android.R.id.text1), withText("deck_1"), isDisplayed()));
        appCompatCheckedTextView.perform(click());

        ViewInteraction fieldEditText3 = onView(
                allOf(withId(R.id.id_note_editText), withContentDescription("Front")));
        fieldEditText3.perform(scrollTo(), replaceText("pre"), closeSoftKeyboard());

        ViewInteraction fieldEditText4 = onView(
                allOf(withId(R.id.id_note_editText), withContentDescription("Back")));
        fieldEditText4.perform(scrollTo(), replaceText("res"), closeSoftKeyboard());

        ViewInteraction actionMenuItemView2 = onView(
                allOf(withId(R.id.action_save), withContentDescription("Save"), isDisplayed()));
        actionMenuItemView2.perform(click());

        ViewInteraction appCompatImageButton2 = onView(
                allOf(withContentDescription("Navigate up"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton2.perform(click());

        ViewInteraction linearLayout = onView(
                allOf(withId(R.id.counts_layout),
                        withParent(allOf(withId(R.id.DeckPickerHoriz),
                                withParent(withId(R.id.files)))),
                        isDisplayed()));
        linearLayout.perform(click());

        ViewInteraction textView2 = onView(
                allOf(withId(R.id.studyoptions_total_new), withText("1"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.studyoptions_deckcounts),
                                        1),
                                1),
                        isDisplayed()));
        //textView2.check(matches(withText("1")));

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
