package com.ichi2.anki;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class EstudiarDeckTest {

    @Rule
    public ActivityTestRule<IntentHandler> mActivityTestRule = new ActivityTestRule<>(IntentHandler.class);

    @Test
    public void estudiarDeckTest() {
        ViewInteraction linearLayout = onView(
                allOf(withId(R.id.counts_layout),
                        withParent(allOf(withId(R.id.DeckPickerHoriz),
                                withParent(withId(R.id.files)))),
                        isDisplayed()));
        linearLayout.perform(click());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.studyoptions_start), withText("Study"),
                        withParent(allOf(withId(R.id.bottom_area_layout),
                                withParent(withId(R.id.studyoptions_mainframe)))),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction linearLayout2 = onView(
                allOf(withId(R.id.flashcard_layout_flip),
                        withParent(allOf(withId(R.id.answer_options_layout),
                                withParent(withId(R.id.bottom_area_layout)))),
                        isDisplayed()));
        linearLayout2.perform(click());

        ViewInteraction linearLayout3 = onView(
                allOf(withId(R.id.flashcard_layout_ease2),
                        withParent(allOf(withId(R.id.answer_options_layout),
                                withParent(withId(R.id.bottom_area_layout)))),
                        isDisplayed()));
        linearLayout3.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction frameLayout = onView(
                allOf(withId(R.id.touch_layer),
                        withParent(allOf(withId(R.id.flashcard_frame),
                                withParent(withId(R.id.front_frame)))),
                        isDisplayed()));
        frameLayout.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction frameLayout2 = onView(
                allOf(withId(R.id.touch_layer),
                        withParent(allOf(withId(R.id.flashcard_frame),
                                withParent(withId(R.id.front_frame)))),
                        isDisplayed()));
        frameLayout2.perform(click());

        ViewInteraction frameLayout3 = onView(
                allOf(withId(R.id.touch_layer),
                        withParent(allOf(withId(R.id.flashcard_frame),
                                withParent(withId(R.id.front_frame)))),
                        isDisplayed()));
        frameLayout3.perform(click());

        ViewInteraction frameLayout4 = onView(
                allOf(withId(R.id.touch_layer),
                        withParent(allOf(withId(R.id.flashcard_frame),
                                withParent(withId(R.id.front_frame)))),
                        isDisplayed()));
        frameLayout4.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction linearLayout4 = onView(
                allOf(withId(R.id.flashcard_layout_flip),
                        withParent(allOf(withId(R.id.answer_options_layout),
                                withParent(withId(R.id.bottom_area_layout)))),
                        isDisplayed()));
        linearLayout4.perform(click());

        ViewInteraction frameLayout5 = onView(
                allOf(withId(R.id.touch_layer),
                        withParent(allOf(withId(R.id.flashcard_frame),
                                withParent(withId(R.id.front_frame)))),
                        isDisplayed()));
        frameLayout5.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction frameLayout6 = onView(
                allOf(withId(R.id.touch_layer),
                        withParent(allOf(withId(R.id.flashcard_frame),
                                withParent(withId(R.id.front_frame)))),
                        isDisplayed()));
        frameLayout6.perform(click());

        ViewInteraction frameLayout7 = onView(
                allOf(withId(R.id.touch_layer),
                        withParent(allOf(withId(R.id.flashcard_frame),
                                withParent(withId(R.id.front_frame)))),
                        isDisplayed()));
        frameLayout7.perform(click());

        ViewInteraction frameLayout8 = onView(
                allOf(withId(R.id.touch_layer),
                        withParent(allOf(withId(R.id.flashcard_frame),
                                withParent(withId(R.id.front_frame)))),
                        isDisplayed()));
        frameLayout8.perform(click());

        ViewInteraction frameLayout9 = onView(
                allOf(withId(R.id.touch_layer),
                        withParent(allOf(withId(R.id.flashcard_frame),
                                withParent(withId(R.id.front_frame)))),
                        isDisplayed()));
        frameLayout9.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction linearLayout5 = onView(
                allOf(withId(R.id.flashcard_layout_ease2),
                        withParent(allOf(withId(R.id.answer_options_layout),
                                withParent(withId(R.id.bottom_area_layout)))),
                        isDisplayed()));
        linearLayout5.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction linearLayout6 = onView(
                allOf(withId(R.id.flashcard_layout_flip),
                        withParent(allOf(withId(R.id.answer_options_layout),
                                withParent(withId(R.id.bottom_area_layout)))),
                        isDisplayed()));
        linearLayout6.perform(click());

        ViewInteraction linearLayout7 = onView(
                allOf(withId(R.id.flashcard_layout_ease3),
                        withParent(allOf(withId(R.id.answer_options_layout),
                                withParent(withId(R.id.bottom_area_layout)))),
                        isDisplayed()));
        linearLayout7.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction linearLayout8 = onView(
                allOf(withId(R.id.flashcard_layout_flip),
                        withParent(allOf(withId(R.id.answer_options_layout),
                                withParent(withId(R.id.bottom_area_layout)))),
                        isDisplayed()));
        linearLayout8.perform(click());

        ViewInteraction linearLayout9 = onView(
                allOf(withId(R.id.flashcard_layout_ease2),
                        withParent(allOf(withId(R.id.answer_options_layout),
                                withParent(withId(R.id.bottom_area_layout)))),
                        isDisplayed()));
        linearLayout9.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction linearLayout10 = onView(
                allOf(withId(R.id.flashcard_layout_flip),
                        withParent(allOf(withId(R.id.answer_options_layout),
                                withParent(withId(R.id.bottom_area_layout)))),
                        isDisplayed()));
        linearLayout10.perform(click());

        ViewInteraction linearLayout11 = onView(
                allOf(withId(R.id.flashcard_layout_ease2),
                        withParent(allOf(withId(R.id.answer_options_layout),
                                withParent(withId(R.id.bottom_area_layout)))),
                        isDisplayed()));
        linearLayout11.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction textView = onView(
                allOf(withId(R.id.deckpicker_new), withText("0"),
                        childAtPosition(
                                allOf(withId(R.id.counts_layout),
                                        childAtPosition(
                                                withId(R.id.DeckPickerHoriz),
                                                2)),
                                0),
                        isDisplayed()));
        textView.check(matches(withText("0")));

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
